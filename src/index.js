import * as React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {store} from './store';
import Users from './users';
import Posts from './posts';
import './styles.css';
export class App extends React.Component {
    render(){
        return(
            <Provider store={store}>
                <div className="grid">
                    <Users />
                    <Posts />
                </div>
            </Provider>
        )
    }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />,rootElement)