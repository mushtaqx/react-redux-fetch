import React from 'react';
import {connect} from 'react-redux';
import {getPosts} from './actions';
class Posts extends React.Component {
    renderProps(){
        const _posts = this.props.posts;
        return _posts !== (null||undefined) && _posts.length > 0 
        ? _posts.map((post)=>{
            return(
                <div key={post.id} className="post-card">
                <p>
                    <span className="id">{post.id}</span>
                    <br />
                    Title: {post.title}
                </p>
            </div>
            )
        }) : <div className="alert">no posts loaded yet</div>;
    }
    render(){
        return(
            <div className="wrapper">
                {this.renderProps()}
                <button onClick={getPosts.bind(this)}>GET POSTS</button>
            </div>
        )
    }
}

export const stateToProps = state => ({
    posts: state.postReducer.payload.posts
})
export default connect(stateToProps)(Posts);